package core;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.junit.Test;


public class ReadGitRepositoryTest {

	private final File repoPath = new File("sample_repos/sample01");
	private final GitRepository repository = new GitRepository(repoPath);
	
	@Test
	public void shouldFindHead() throws Exception {
		assertEquals("refs/heads/master",repository.getHeadRef());
	}

}
